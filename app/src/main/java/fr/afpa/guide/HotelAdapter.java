package fr.afpa.guide;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.afpa.guide.models.Hotel;

public class HotelAdapter extends ArrayAdapter<Hotel> {
    public HotelAdapter(Context context, int resource, List<Hotel> objects) {
        super(context, resource, objects);

    }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            // utilise le layout R.layout.item_restaurant
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_restaurant, null);

            TextView textViewTitle = convertView.findViewById(R.id.textViewTitle);
            TextView textViewCategory = convertView.findViewById(R.id.textViewCategory);

            Hotel item = getItem(position);

            textViewTitle.setText(item.getName());
            textViewCategory.setText(item.getCategory());

            return convertView;
        }
}
