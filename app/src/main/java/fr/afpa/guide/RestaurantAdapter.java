package fr.afpa.guide;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.afpa.guide.models.Restaurant;

public class RestaurantAdapter extends ArrayAdapter<Restaurant> {

    private int resId;      // comme dit en dessous permet de rendre resource visible

    public RestaurantAdapter(Context context, int resource, List<Restaurant> objects) {
        super(context, resource, objects);      // le super permet de remonter les informations directement
        resId = resource;                       // la ressource est dispo que dans ce bloc. Grace a la declaration de resId au dessus
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) { // des qu il ya une chose a afficher ca va passer par le getView.  Le parent sert pas souvent que quand il y a sous liste

        ViewHolder myViewHolder; // Declaration

        if (convertView == null) {  // le fait d utiliser cette méthode permet de faire cela que quand le convertview est nul
            // utilise le layout R.layout.item_restaurant
            convertView = LayoutInflater.from(getContext()).inflate(resId, null); // de base le convertView vaut nul. L inflate permet d ajouter un element. on evite d'écrire R.Id... grace a la declaration de desId

            myViewHolder = new ViewHolder();  // création de l instance = utilisation de la mémoire

            myViewHolder.textViewTitle = convertView.findViewById(R.id.textViewTitle);          // creation des 2 views title et category et on va les chercher avec le convertView. find
            myViewHolder.textViewCategory = convertView.findViewById(R.id.textViewCategory);

            convertView.setTag(myViewHolder);  // enregistrement de l'instance de ViewHolder. la méthode setTag permet de stocker des objets. On l enregistre dans convertView car on a ensuite un return du converView
        } else  {
            myViewHolder = (ViewHolder) convertView.getTag();    // s utilise principalement qur android ici on récupére recupérer les objets. en ayant fait alt entrée il propose de cast cela met (viewHolder) et enleve l erreur
        }


        Restaurant item = getItem(position);        // on récupére l element (item) et on le met a jout avec getItem. Position est l indice de l element

        myViewHolder.textViewTitle.setText(item.getName());          // appel aux 2 views crées et on modifie leur valeur grace a set. Les getteurs permettent de récupérer
        myViewHolder.textViewCategory.setText(item.getCategory());


        return convertView;
    }

    private class ViewHolder {
        TextView textViewTitle; // Permet de dire que ViewHolder necessite un titre et une categorie
        TextView textViewCategory;
    }
}
