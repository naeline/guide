package fr.afpa.guide;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppActivity {   // extends permet d'hériter des propriétés du ficher que l'on met après

    // Initialisation de variable
    private Timer myTimer;

    @Override                                           // généré par l ide en tapant entrer OVERRIDE = SURCHARGE
    protected void onCreate(Bundle savedInstanceState) {// Lors de l'héritage récupération des données maiq qu'on peut modifier
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTimer = new Timer();                          // instance de classe
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.e("Main", "Lancer HomeActivity");   // afficher dans la console logcat en erreur vu que le e

                Intent myIntent = new Intent(MainActivity.this, HomeActivity.class);   // INTENT est une class qui sert de paramétrage
                startActivity(myIntent);        // demarre l'INTENT     // si on veut récupérer la class dans la quelle on est plutot mettre son nom.this que this tout court car sinon le this récupère l objet dans lequel il est directement implémenté
                finish(); // permet de sortir de l'appli quand on revient sur le premier écran autre maniere dans le manifest mettre android:noHistory="true" pour la page d accueil
            }
        }, 2000);

        // * * * * * AUTRE MANIERE DE FAIRE
        // Déclaration d'un objet de type Timertask
        //
        //TimerTask monTimerTask = new TimerTask() {
        //   @Override
        //    public void run() {
        //
        //    }
        //};
        //myTimer.schedule(monTimerTask, 2000);
    }
}
