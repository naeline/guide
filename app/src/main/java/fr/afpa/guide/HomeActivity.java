package fr.afpa.guide;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class HomeActivity extends AppActivity {

    // partie globale

    // Déclaration des propriétés ou variable
    private Button buttonRestaurant;
    private Button buttonHotel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);  // permet d'afficher le layout

        // récupération des views (vues) à faire après la declaration du layout

        buttonRestaurant = findViewById(R.id.buttonRestaurant);     // dans android coté java le @ vaut R le reste ".id." c'est la doc qui l'a donné en info bulle sinon faire ctrl P
        buttonHotel = findViewById(R.id.buttonHotel);


        buttonRestaurant.setOnClickListener(new View.OnClickListener() {    // ajout d el'évenement au click avec setOnClickListener, la doc demande une class donc on met new
            @Override
            public void onClick(View v) {
                Intent intentResto = new Intent(HomeActivity.this, ListingActivity.class);   // INTENT est une class qui sert de paramétrage

                intentResto.putExtra("isRestaurant", true);     // putExtra sert comme post en php il va renvoyer une valeur ici oui c est un resto

                startActivity(intentResto);        // demarre l INTENT
            }
        });

        buttonHotel.setOnClickListener(new View.OnClickListener() {    // ajout d el'évenement au click avec setOnClickListener, la doc demande une class donc on met new
            @Override
            public void onClick(View v) {
                Intent intentHotel = new Intent(HomeActivity.this, ListingActivity.class);   // INTENT est une class qui sert de paramétrage

                intentHotel.putExtra("isRestaurant", false);    // ici non donc il enverra la page hotel attention on garde bien la meme clé

                startActivity(intentHotel);                 // si on veut récupérer la class dans la quelle on est plutot mettre son nom.this que this tout court car sinon le this récupère l objet dans lequel il est directement implémenté
            }
        });
    }
}
