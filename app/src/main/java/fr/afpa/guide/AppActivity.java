package fr.afpa.guide;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

public class AppActivity extends AppCompatActivity {

    protected void showToast(String message) {
        Toast.makeText(AppActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home) {
            finish(); // fermeture de l'activity courante
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!(this instanceof HomeActivity) && getSupportActionBar() != null) {

            // affichage de la flèche de retour
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}



