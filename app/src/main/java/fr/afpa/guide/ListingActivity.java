package fr.afpa.guide;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;



import fr.afpa.guide.models.Hotel;
import fr.afpa.guide.models.Restaurant;

public class ListingActivity extends AppActivity {

    // déclaration
    private TextView textViewTitle;
    private ListView listViewData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);

        // récupération des vues
        textViewTitle = findViewById(R.id.textViewTitle);   // dans android coté java le @ vaut R le reste ".id." c'est la doc qui l'a donné en info bulle sinon faire ctrl P
        listViewData = findViewById(R.id.listViewData);     // permet de pouvoir récupérer grace a l id


        /*// EXEMPLE
        `creer un tableau de string
        String[] restaurants = {"Mac DO","Quick","KFC","Burger King"};

        // gestion de l'affichage des items  grace a la methode setAdapter
        ListViewData.setAdapter(new ArrayAdapter<String>(ListingActivity.this, // recupere le contexte
                android.R.layout.simple_list_item_1, // layout on utilise android pour récupérer des elements du framework android
                restaurants));                         // recupere les donnes dans notre tableau
*/
        // Récupération de la clé isRestaurant
        if (getIntent().getExtras() != null){   // pareil que isset de php on vérifie si different de nul avec getIntent en récuperant les données grace a getExtras

            boolean isRestaurant = getIntent().getExtras().getBoolean("isRestaurant");      // apres boolean le isRestaurant est juste une variable elle peut avoir n importe quel nom
                                                                                                // par contre la key isRestaurant est très importante
            if (isRestaurant) {
                //  AFFICHAGE DES RESTAURANTS


                textViewTitle.setText(R.string.listing_restaurant_title);

                // création d'une liste de restaurant
                final List<Restaurant> restaurantList = new ArrayList<>();      // le deuxieme final pour faire le lien
                restaurantList.add(new Restaurant("Mac Do","Fast Food","McDO@mcdonalds.com","0145454545","http://www.mcdonalds.com","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSevnhu2nmrLmpkfbEjnBp1EUxb9JZBLTOYVw9daQftdi-p7hGq"));
                restaurantList.add(new Restaurant("KFC","Fast Food","kfc@poulet.com","0136987412","http://www.kfc.com","https://upload.wikimedia.org/wikipedia/fr/thumb/b/bf/KFC_logo.svg/langfr-280px-KFC_logo.svg.png"));
                restaurantList.add(new Restaurant("Burger King","Fast Food","burgerking@burger.com","0158741236","http://www.burgerking.com","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-1tAMu_KPG3xfeGhYFvv_yW4-6czx-9uphZBTXJuyAS2eJmq6MA"));
                restaurantList.add(new Restaurant("Nonna & Nonno","Resto italien","nonna-nonno@yahoo.fr","0148622448","http://www.nonna-nonno.fr","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyOah9O2lZtM-_ZbU3XuDNTyAkC-YyjXDPWbV72M2ZOvqqQJVY"));
                restaurantList.add(new Restaurant("Paul","Boulangerie","paul@free.fr","0142266884","http://www.paul.fr","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSI-eD1luAcLTuuZImhPRnwb3J7kM1SRWqHckvukZ2raNZde806RA"));

                listViewData.setAdapter(new RestaurantAdapter(ListingActivity.this, R.layout.item_restaurant, restaurantList));  // la méthode setAdapter permet d'afficher ce qu'il faut. RestaurantAdapter permet de creer la liste

                listViewData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        final Restaurant objetRestaurant = restaurantList.get(position);        // utilisation de final pour récupérer restaurantList qui est en dehors de la fonction, on met final aussi au dessus

                        Intent intentDetailResto = new Intent(ListingActivity.this, DetailsActivity.class);

                        intentDetailResto.putExtra("name", objetRestaurant.getName());
                        intentDetailResto.putExtra("category", objetRestaurant.getCategory());
                        intentDetailResto.putExtra("email", objetRestaurant.getEmail());
                        intentDetailResto.putExtra("phone", objetRestaurant.getPhone());
                        intentDetailResto.putExtra("url", objetRestaurant.getUrl());
                        intentDetailResto.putExtra("image", objetRestaurant.getImage());
                        startActivity(intentDetailResto);
                    }
                });


            } else {
                // AFFICHAGE DES HOTELS
                textViewTitle.setText(R.string.listing_hotel_title);

                // Creation d'une liste d'hotel
                final List<Hotel> hotelList = new ArrayList<>();
                hotelList.add(new Hotel("Novotel", "Luxe", "novotel@orange.fr", "0898969896", "novotel-france.fr", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnFtByaGhw4Auwd2mxVrf520M44G98XDHzrlLG2FDYUtHtlRMq"));
                hotelList.add(new Hotel("Ibis", "Discount", "ibis@gmail.com", "0895742687", "ibis-hotel.com", "https://yt3.ggpht.com/a-/AAuE7mAgtatRhNh8iSXDAIa6Ub7leTb2REKDI6FVSA=s900-mo-c-c0xffffffff-rj-k-no"));
                hotelList.add(new Hotel("Hilton", "Luxe", "hilton@hotmail.fr", "0874569885", "hilton.fr", "https://www3.hilton.com/resources/media/hi/GLSGIHF/en_US/img/shared/full_page_image_gallery/main/HH_exteriornight_2_1270x560_FitToBoxSmallDimension_Center.jpg"));
                hotelList.add(new Hotel("Sofitel", "Luxe", "sofitel@laposte.net", "0987452136", "sofitel.com", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQN25yBFyTCtFAg-g3x4SY-nu4sKHzTjWM5kGnsqeD1jULFcaBk"));
                hotelList.add(new Hotel("Mercure", "Standard", "mercure@aol.fr", "0745896321", "mercure.fr", "https://upload.wikimedia.org/wikipedia/commons/5/5e/Mercure_Logo_2013.png"));

                listViewData.setAdapter(new HotelAdapter(ListingActivity.this, R.layout.item_hotel, hotelList));

                listViewData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        final Hotel objetHotel = hotelList.get(position);

                        Intent intentDetailResto = new Intent(ListingActivity.this, DetailsActivity.class);

                        intentDetailResto.putExtra("name", objetHotel.getName());
                        intentDetailResto.putExtra("category", objetHotel.getCategory());
                        intentDetailResto.putExtra("email", objetHotel.getEmail());
                        intentDetailResto.putExtra("phone", objetHotel.getPhone());
                        intentDetailResto.putExtra("url", objetHotel.getUrl());
                        intentDetailResto.putExtra("image", objetHotel.getImage());
                        startActivity(intentDetailResto);
                    }
                });

            }
        }





    }
}
