package fr.afpa.guide;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppActivity {

    // déclaration
    private TextView textViewTitle;
    private TextView textViewCategory;
    private Button buttonMail;
    private Button buttonPhone;
    private Button buttonWeb;
    private ImageView imageView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewCategory = (TextView) findViewById(R.id.textViewCategory);
        buttonMail = (Button) findViewById(R.id.buttonMail);
        buttonPhone = (Button) findViewById(R.id.buttonPhone);
        buttonWeb = (Button) findViewById(R.id.buttonWeb);
        imageView = (ImageView) findViewById(R.id.imageView);





        if(getIntent().getExtras() != null) {
            String name = getIntent().getExtras().getString("name");
            String category = getIntent().getExtras().getString("category");
            String email = getIntent().getExtras().getString("email");
            String phone = getIntent().getExtras().getString("phone");
            String url = getIntent().getExtras().getString("url");
            String image = getIntent().getExtras().getString("image");




            // ensuite pour afficher les informations sur chaque View (TextView, Button ...)
            textViewTitle.setText(name);
            textViewCategory.setText(category);
            buttonMail.setText(email);
            buttonPhone.setText(phone);
            buttonWeb.setText(url);
            Picasso.get().load(image)
                    .fit().centerCrop()
                    .into(imageView);
        }

    }

    public void sendEmail(View view) {
        Intent intentEmail = new Intent(Intent.ACTION_SEND);

        intentEmail.setType("message/rfc822");  // message... adresse de messaqgerie

        intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[] { buttonMail.getText().toString(), "mouloud.mala@gmail.com"});
        intentEmail.putExtra(Intent.EXTRA_SUBJECT, "le sujet");     // action supplémentaire
        intentEmail.putExtra(Intent.EXTRA_TEXT, "le message");      // action supplémentaire
        startActivity(intentEmail);
        //startActivity(Intent.createChooser(intentEmail, "Partage :"));       à mettre si il n' a pas d appli qui peut envoyer un mail pareil pour les autres boutons
        // EXTRA_CC pour une copie
        // EXTRA_BCC pour une copie cachée
    }


    public void call(View view) { // ajouter permission dans manifest CALL PHONE
        Intent intentPhone = new Intent(Intent.ACTION_CALL);

        intentPhone.setData(Uri.parse("tel:"+buttonPhone.getText().toString()));

        // DEMANDE DE PERMISSION APPEL
        if (ContextCompat.checkSelfPermission(DetailsActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{ Manifest.permission.CALL_PHONE }, 100);
            }   //exécuté qu a partir de l api 23 grace a akt entree
        }   else {
                startActivity(intentPhone);
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {// accepté
                // lancer l appel
                buttonPhone.performClick(); //methode performClick simule un click sur le bouton
            } else { // refusé
                // aficher une erreur
                showToast("Permission refusé");   // Toast permet d afficher une infobulle
            }
        }
    }

    public void showWebSite(View view) {
        Intent intentUrl = new Intent(Intent.ACTION_VIEW);      // INTENT IMPLICITE     contante toujouyrs en majuscule et separé par underscore. Ici c est pour aller voir le site

        intentUrl.setData(Uri.parse(buttonWeb.getText().toString()));

        startActivity(intentUrl);
    }
}
